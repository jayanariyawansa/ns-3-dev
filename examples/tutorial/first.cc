/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"

// Default Network Topology
//
//       10.1.1.0
// n0 -------------- n1
//    point-to-point
//
 
using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("FirstScriptExample");

int
main (int argc, char *argv[])
{
  CommandLine cmd (__FILE__);
  cmd.Parse (argc, argv);
  
  Time::SetResolution (Time::NS); // smallest time interval
  LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO); //enables log from these components into info level 
  LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);

//Creates two computers
  NodeContainer nodes;
  nodes.Create (2);
// Helps connect nodes with 5mbps with a delay of 2ms between each channel (node)
  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));
// creates the "network"
  NetDeviceContainer devices;
  devices = pointToPoint.Install (nodes);
// installs internet stack (TCP,IP,...)
  InternetStackHelper stack;
  stack.Install (nodes);
// tells the helper to allocate IP address from 10.1.1.0 from subnet mask ...
  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");
// performs the actual IP assignment using the netdevicecontainer
  Ipv4InterfaceContainer interfaces = address.Assign (devices);


/* 

                                  Applications (UdpEchoServerHelper/UdpEchoClientHelper)

*/
  //Declares the application helper
  UdpEchoServerHelper echoServer (9);
  //echo Server application is created and attached to one of the nodes via smart pointer
  ApplicationContainer serverApps = echoServer.Install (nodes.Get (1));
  // Applications require a start (stop is optional). Application will start 1 sec into simulation and end at 10 seconds on simulation
  serverApps.Start (Seconds (1.0));
  serverApps.Stop (Seconds (10.0));


 // creates a application helper that is set to remote address of server's address and port (1)
  UdpEchoClientHelper echoClient (interfaces.GetAddress (1), 9);
  
  echoClient.SetAttribute ("MaxPackets", UintegerValue (1)); // max number of packets for simulation
  echoClient.SetAttribute ("Interval", TimeValue (Seconds (1.0))); // time intervals between packet
  echoClient.SetAttribute ("PacketSize", UintegerValue (1024)); // packet size 
  //echo client application is created and attached to one of the nodes via smart pointer
  ApplicationContainer clientApps = echoClient.Install (nodes.Get (0));
  // start client application 2 secs in the simulation 
  clientApps.Start (Seconds (2.0));
  clientApps.Stop (Seconds (10.0));

  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
